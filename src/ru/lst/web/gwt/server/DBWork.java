package ru.lst.web.gwt.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DBWork {


    public static String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	public Connection db_con = null;

    //url = "jdbc:oracle:thin:@" + server + ":" + port + ":" + sid;

    public Connection dbConnect(String DB_URL, String DB_LOGIN, String DB_PASS)
        {
            Locale.setDefault(Locale.ENGLISH); //ORA-00604 ORA-12705 -1 DAY
            Connection dbConnection = null;
            try {
                Class.forName(DB_DRIVER);
            } catch (ClassNotFoundException e) {
                System.out.println(e.getMessage());
            }
            try {        
                dbConnection = DriverManager.getConnection(DB_URL, DB_LOGIN, DB_PASS);
                if(!dbConnection.isClosed()) {
                    System.out.println("Connected to DB successfully!");
                }
                if(dbConnection.isClosed()) {
                    System.out.println("Connected to DB not successfully");
                }
                return dbConnection;
            }   catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            return dbConnection;
        }
    
    public static String[] sqlExecute(Connection con, List<String> inputStrings){
    	ArrayList<String> labelsAndValues = new ArrayList<String>();
    	List<String> finalResult = new ArrayList<String>();
    	Statement st = null;
    	ResultSet rs = null;
    	String labels = "";
		String value = "";
		try {
			st = con.createStatement();
			finalResult.clear();
			for (int i = 0; i < inputStrings.size(); i++) {
				finalResult.add(inputStrings.get(i));   //add sqlRequest for logs
				rs = st.executeQuery(inputStrings.get(i));
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
					
				//add column names from sqlAnswer
				//ResultSet data starts from "1"
				for (int k = 1; k < columnCount + 1; k++ ) {
				  String name = rsmd.getColumnName(k);
				  labels = labels + name + " ";
				}
				
				labelsAndValues.add(labels);
				labels = "";
				//add values from sqlAnswer
				while (rs.next())
				{
					for (int j = 1; j<columnCount+1; j++) {
						value = value + rs.getString(j) + " ";
					}
					
					labelsAndValues.add(value);
					value = " ";
				}
				//finalResult is sqlRequest plus sqlResponse
				finalResult.addAll(labelsAndValues);  
				labelsAndValues.clear();
				rs.close();
			}
			
		st.close();
		String[] fin = finalResult.toArray(new String[finalResult.size()]);
		return fin;
		} 
		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
    }
}