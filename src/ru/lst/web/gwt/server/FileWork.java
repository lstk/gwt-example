package ru.lst.web.gwt.server;

import java.io.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

public class FileWork {

	public static List<String> file_list = new ArrayList<String>();

	public static List<String> openFile(String file_name) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file_name));
		String line;
		List<String> lines = new ArrayList<String>();
		while ((line = reader.readLine()) != null) {
			lines.add(line);
		}
		reader.close();
		return lines;
	}

	//combine sql request and separate them by "\n/\n"
	public static List<String> clearList(List<String> inputList)
	{
		List<String> outputList = new ArrayList<String>();
		int counter = 0;
		outputList.add(counter, inputList.get(0));

		for (int i = 0; i <= inputList.size()-2; i++)
		{
			if (!inputList.get(i+1).equals("/"))
			{
				outputList.set(counter, (outputList.get(counter) + " " + inputList.get(i+1) + " "));
			}
			if (inputList.get(i+1).equals("/"))
			{
				counter++;
				outputList.add(counter, "");
			}
		}
		inputList.clear();


		return outputList;
	}

}
