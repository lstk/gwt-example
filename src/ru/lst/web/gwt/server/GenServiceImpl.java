package ru.lst.web.gwt.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.lst.web.gwt.client.GenService;

public class GenServiceImpl extends RemoteServiceServlet implements GenService {

	private static final long serialVersionUID = 5907919570615381302L;
	
	public Connection conDB = null;

	public String[] openFile() {
		List<String> line = new ArrayList<String>();
		try {
			URL myURL = new URL("http://127.0.0.1:8888/SQL.txt");
			BufferedReader in = new BufferedReader(
	                new InputStreamReader(myURL.openStream()));
	                String inputLine;
	                while ((inputLine = in.readLine()) != null)
	                    line.add(inputLine);
	                in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] outputString = null;
		outputString = line.toArray(new String[line.size()]);
		return outputString;
	}
	
	public void connectDB(String login, String password, String server){
		Connection dbCon = null;
		DBWork dbTest = new DBWork();
        dbCon = dbTest.dbConnect("jdbc:oracle:thin:@//" + server + 
        		":1521/XE", login, password);
        conDB = dbCon; //save connection
	}
	
	public void saveToLogFile(String input){
		FileWriter writeFile = null;
		try {
			File logFile = new File("log.txt");
			writeFile = new FileWriter(logFile, true);
			writeFile.write(input + " " + "\r\n");
			writeFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveToSqlFile(String[] input){
		FileWriter writeFile = null;
		try {
			File sqlFile = new File("SQL.txt");
			writeFile = new FileWriter(sqlFile, false);
			for (int i = 0; i< input.length; i++)
			{
				writeFile.write(input[i] + "\r\n");
			}
			writeFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public String[] execSql(){
		
		List<String> sqlCommands = new ArrayList<String>();
		try {
			URL myURL = new URL("http://127.0.0.1:8888/SQL.txt");
			BufferedReader in = new BufferedReader(
	                new InputStreamReader(myURL.openStream()));
	                String inputLine;
	                while ((inputLine = in.readLine()) != null)
	                	sqlCommands.add(inputLine);
	                in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		sqlCommands = FileWork.clearList(sqlCommands); 
		String[] fin = DBWork.sqlExecute(conDB, sqlCommands);

		return fin;
	}
	
	
}