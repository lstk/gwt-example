package ru.lst.web.gwt.client;



import com.google.gwt.user.client.rpc.AsyncCallback;


public interface GenServiceAsync {

	void saveToSqlFile(String[] sta, AsyncCallback<Void> asyncCallback);
	void openFile(AsyncCallback<String[]> asyncCallback);
	void connectDB(String login, String password, String server,
			AsyncCallback<Void> asyncCallback);
	void saveToLogFile(String st, AsyncCallback<Void> asyncCallback);
	void execSql(AsyncCallback<String[]> asyncCallback);
}
