package ru.lst.web.gwt.client;

//import com.google.appengine.labs.repackaged.com.google.common.base.Strings;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;


@RemoteServiceRelativePath("GenService")
public interface GenService extends RemoteService {
	String[] openFile();
	void connectDB(String login, String password, String server);
	void saveToLogFile(String st);
	void saveToSqlFile(String[] sta);
	String[] execSql();
	


	/**
	 * Utility/Convenience class.
	 * Use GenService.App.getInstance() to access static instance of GenServiceAsync
	 */
	public static class App {
		private static final GenServiceAsync ourInstance = (GenServiceAsync) GWT.create(GenService.class);

		public static GenServiceAsync getInstance() {
			return ourInstance;
		}
	}



















}
