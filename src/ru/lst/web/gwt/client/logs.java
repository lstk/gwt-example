package ru.lst.web.gwt.client;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextArea;

public class logs {
	
	public static void logging(String input, TextArea ta){
		String st = null;
		Date date = new Date();
		DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy/MM/dd HH:mm:ss");
		//-240 min = -3hours timezone 
		String NowTime = dtf.format(date, TimeZone.createTimeZone(-240)); 
		ta.setText(ta.getText() + "\n" + NowTime + " " + input);
		st = (NowTime + " " + input);
		
		//вызываем на сервере ф-цию сохр. в файл
		GenService.App.getInstance().saveToLogFile(st, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Can't save logs to file" + caught);
				
			}

			@Override
			public void onSuccess(Void result) {
				
			}
			
			
		});
		
	}

}
