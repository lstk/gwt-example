package ru.lst.web.gwt.client;


import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;



public class Gwt_example implements EntryPoint {
	
	public String[] getTAText(TextArea ta){
		String[] lines = ta.getText().split("\\n");//
	    return lines;
	}
	
	

	public void onModuleLoad() {
		Button buttonOpen = new Button("Открыть файл");
		Button buttonSave = new Button("Сохранить в файл");
		Button buttonExec = new Button("Выполнить SQL");
		final Button buttonConnect = new Button("Connect to Oracle");
		
		final TextBox loginTB = new TextBox();
		final TextBox serverTB = new TextBox();
		final PasswordTextBox passTB = new PasswordTextBox();
		final TextBox iterTB = new TextBox();
		
		RootPanel.get("login").add(loginTB);
		RootPanel.get("pass").add(passTB);
		RootPanel.get("server").add(serverTB);
		RootPanel.get("connect").add(buttonConnect);
		
		RootPanel.get("openbutton").add(buttonOpen);
		RootPanel.get("savebutton").add(buttonSave);
		RootPanel.get("execbutton").add(buttonExec);
		RootPanel.get("iterations").add(iterTB);
		final TextArea textArea = new TextArea();
		final TextArea logArea = new TextArea();
		textArea.setSize("100%", "100%");
		logArea.setSize("100%", "100%");
		iterTB.setText("1");
		textArea.setText("SELECT * FROM person");
		//SQLFile and LOGFile are div's from html
		RootPanel.get("SQLFile").add(textArea);   
		RootPanel.get("LOGFile").add(logArea);



		buttonOpen.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				GenService.App.getInstance().openFile(new AsyncCallback<String[]>() {
					
					public void onFailure(Throwable caught) {
						Window.alert("Can't open file. Reason: " + caught);
					}

					public void onSuccess(String[] result) {
						textArea.setText("");
						for(int i=0; i<result.length; i++){
								textArea.setText(textArea.getText() + result[i]);
								textArea.setText(textArea.getText() + "\n" );
						}
					}
				});
			}
		});
		
		buttonConnect.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				GenService.App.getInstance().connectDB(loginTB.getText(), passTB.getText(), 
						serverTB.getText(),   new AsyncCallback<Void>() {
					
					public void onFailure(Throwable caught) {
						Window.alert("No connection to server: " + caught);
						logs.logging("No connection to server: " + caught, logArea);
					}

					public void onSuccess(Void result) {
							Element testDiv = Document.get().getElementById("connect2");
							testDiv.setInnerHTML("<h1>Connected to DB successfully!</h1>");
							logs.logging("Connected to DB " + serverTB.getText() + "/XE", logArea);
						}
					}
				);
			}
		});
		
		buttonExec.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String Alarm = iterTB.getText();
				int iterations = Integer.parseInt(Alarm);
				Window.alert("SQL will exec " + Alarm + " times");
				for (int i = 0; i < iterations; i++){
					GenService.App.getInstance().execSql(new AsyncCallback<String[]>(){
	
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Can't exec SQL from file: " + caught);
						}
	
						@Override
						public void onSuccess(String[] result) {
							for (int i = 0; i<result.length; i++){
								logs.logging(result[i], logArea);
							}
						}
					});
				}
			}
		});
		
		buttonSave.addClickHandler(new ClickHandler(){
			
			@Override
			public void onClick(ClickEvent event) {
				GenService.App.getInstance().saveToSqlFile(getTAText(textArea), new AsyncCallback<Void>(){
					
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Can't save to file: " + caught);
					}
					
					@Override
					public void onSuccess(Void result) {
						Window.alert("File saved(SQL.txt);");
					}
				});
			}
		});
	}
}

